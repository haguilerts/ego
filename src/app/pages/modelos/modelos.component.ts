import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-modelos',
  templateUrl: './modelos.component.html',
  styleUrls: ['./modelos.component.scss']
})
export class ModelosComponent implements OnInit {
  myOrder:string=''
  constructor() { }

  ngOnInit(): void {
  }
  
  selectOption(filtro:string){
    this.myOrder=filtro
  }
}
