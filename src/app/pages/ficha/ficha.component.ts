import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AgenciaEgoService } from 'src/app/service/agencia-ego.service';
import { CardEco } from 'src/app/shader/models/cardEgo';

@Component({
  selector: 'app-ficha',
  templateUrl: './ficha.component.html',
  styleUrls: ['./ficha.component.scss']
})
export class FichaComponent implements OnInit {
  @Input() card:CardEco;
  constructor( private paramURL:ActivatedRoute,private cardServis:AgenciaEgoService ) { 
    this.card={
      id:-1,
      name:'Hilux DX/SR',
      photo:'../../../../assets/pages/ficha/card.png',
      price:0,
      segment:'',
      thumbnail:'./../../../assets/pages/ficha/card_inf.png',
      year:0
    }
  }

  async ngOnInit(){
   const params = this.paramURL.snapshot.params;
   if (params.idcard) {
     try {
      this.card =(await this.cardServis.getById(params.idcard))[0]
     } catch (error) {
       console.error(error)
     }
   }
  }
  ngOnChanges(changes:SimpleChange){   
   
  }

}
