import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FichaComponent } from './pages/ficha/ficha.component';
import { ModelosComponent } from './pages/modelos/modelos.component';

const routes: Routes = [
  {path:'', pathMatch:'full',redirectTo:'home'},
  {path:'', component: ModelosComponent},
  {path:'ficha', component: FichaComponent},
  {path:'ficha/:idcard', component: FichaComponent},
  { path: '**', redirectTo: 'home', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    initialNavigation: 'enabled',
}),],
  exports: [RouterModule]
})
export class AppRoutingModule { }
