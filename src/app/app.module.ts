import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { ModelosComponent } from './pages/modelos/modelos.component';
import { FichaComponent } from './pages/ficha/ficha.component';
import { CardsComponent } from './components/cards/cards.component';
import { CarrucelComponent } from './components/carrucel/carrucel.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ModelosComponent,
    FichaComponent,
    CardsComponent,
    CarrucelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
