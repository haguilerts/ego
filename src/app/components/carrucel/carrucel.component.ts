
import * as jQuery from 'jquery';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-carrucel',
  templateUrl: './carrucel.component.html',
  styleUrls: ['./carrucel.component.scss']
})
export class CarrucelComponent implements OnInit {
  
  constructor() { 
   
    
  }

  ngOnInit(): void {
    
    setInterval(()=>{
      this.irAdelante();
    },9000)   
    
  }
 

  irAdelante(){
      let sliderSectionFirst = document.querySelectorAll(".slider__section")[0];  
      jQuery('#slider').append(sliderSectionFirst)  
      jQuery('#slider').css('margin-left', '-13%');
     
  }
  irAtras(){
    let sliderSection = document.querySelectorAll(".slider__section");
    let sliderSectionLast = sliderSection[sliderSection.length - 1]
    jQuery('#slider').prepend(sliderSectionLast)     
    jQuery('#slider').css('margin-left', '-13%');
   
  }
}
