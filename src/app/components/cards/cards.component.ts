import { Component, Input, OnInit, SimpleChange } from '@angular/core';
import { Router } from '@angular/router';
import { AgenciaEgoService } from 'src/app/service/agencia-ego.service';
import { CardEco } from 'src/app/shader/models/cardEgo';
import { OrderBy } from './orderBy';
@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {
  @Input() typeCard:string=''
  private allCards:CardEco[]=[]
  cardsTemporary:CardEco[]=[]
  cards:CardEco[];
  ordering:OrderBy;
  constructor(private servisEgo:AgenciaEgoService, private ruta:Router ) {
    this.cards=[{
      id:0,
      name:'',
      photo:'',
      price:0,
      segment:'',
      thumbnail:'',
      year:0
    }]
    this.ordering=new OrderBy(this.cards)
  }
  
  async ngOnInit(){

    try {
      this.allCards= await this.servisEgo.getAll();
      this.ordering=new OrderBy(this.allCards)
      this.cards= this.allCards
      this.cardsTemporary=this.allCards
    } catch (error) {
      console.error(error)
    }
  } 
  
  ngOnChanges(changes:SimpleChange){   
    this.fitrarPor(this.typeCard)   
  }
  fichas(id:any){
 
   if(window.screen.width<698){
     this.ruta.navigate(['/ficha/'+id])
   }
  }
  private definirAutos(tipo:string){
    var auto:CardEco[]=[];
    this.allCards.forEach(e => {
      try {       
        if(e.segment==tipo){         
          auto.push(e)
        }else if(tipo=='todos'){          
          auto=this.allCards
        }
      } catch (error) {
        console.error(error)
      }        
    });
    this.cardsTemporary=auto
    this.cards=auto
  }

  private fitrarPor(option:string){
    this.ordering=new OrderBy(this.cardsTemporary)
    switch(option){
      case 'Price-men_may':
        this.cards=this.ordering.desPrice()
        break;
      case 'Price-may_men':
        this.cards=this.ordering.ascPrice()
        break;
      case 'Year-men_may':
        this.cards=this.ordering.desYear()
        break;
      case 'Year-may_men':
        this.cards=this.ordering.ascYear()
        break;
      case '':
        break;
      default:
        this.definirAutos(option)
        break;
    }  
  }

}
