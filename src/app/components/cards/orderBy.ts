import { CardEco } from "src/app/shader/models/cardEgo";

export class OrderBy{
    vector:CardEco[];
    constructor(private cards:CardEco[]){
        this.vector=cards
    }
    desYear():CardEco[]{
        let tmp, minimo;
        for (let i = 0; i < this.vector.length - 1; i++) {
            minimo = i
            for (let j = i + 1; j < this.vector.length; j++) {
                if (this.vector[minimo].year > this.vector[j].year) {
                    minimo = j
                }
            }
            tmp = this.vector[i] //23
            this.vector[i] = this.vector[minimo] //5
            this.vector[minimo] = tmp //23
            //console.log('vector: ', this.vector[i])
        }
        return this.vector
    }
    ascYear():CardEco[]{
        this.desYear()
        return this.vector.reverse()
    }
    desPrice():CardEco[]{
        let tmp, minimo;
        for (let i = 0; i < this.vector.length - 1; i++) {
            minimo = i
            for (let j = i + 1; j < this.vector.length; j++) {
                if (this.vector[minimo].price > this.vector[j].price) {
                    minimo = j
                }
            }
            tmp = this.vector[i] //23
            this.vector[i] = this.vector[minimo] //5
            this.vector[minimo] = tmp //23
            console.log('vector: ', this.vector[i])
        }
        return this.vector
    }
    ascPrice():CardEco[]{
        this.desPrice()
        return this.vector.reverse()
    }

    private metodoSeleccion() {
        console.log('vector: ', this.vector)
        let tmp;
        let minimo
        for (let i = 0; i < this.vector.length - 1; i++) {
            minimo = i
            for (let j = i + 1; j < this.vector.length; j++) {
                if (this.vector[minimo].id > this.vector[j].id) {
                    minimo = j
                }
            }
            tmp = this.vector[i].id //23
            this.vector[i].id = this.vector[minimo].id //5
            this.vector[minimo].id = tmp //23
            console.log('min: ', minimo, this.vector[i].id)
        }
        //vector.reverse()
        console.log('--------- fin --------')
    }

    private metodoBurbujeo() {
        console.log('--------- inicio --------')
        console.log(' no tiene q ver valores iguales ')
        console.log('vector: ', this.vector)
        let aux;
        for (let i = 0; i < this.vector.length; i++) {
            for (let j = 0; j < this.vector.length - 1; j++) {
                if (this.vector[j].id > this.vector[j + 1].id) {
                    aux = this.vector[j];
                    this.vector[j] = this.vector[j + 1];
                    this.vector[j + 1] = aux;
                }
            }
        }
        //vector.reverse()
        console.log('min: ', this.vector)

        console.log('--------- fin --------')
    }

    private metodoInsercion() {
        console.log('--------- inicio --------')
        let j, aux;
        for (let i = 1; i < this.vector.length; i++) {
            j = i;
            aux = this.vector[i];
            while ((j > 0) && (aux.id < this.vector[j - 1].id)) {
                console.log('aux: ', aux)
                this.vector[j] = this.vector[j - 1];
                j--;
            }
            this.vector[j] = aux;
        }
        console.log('min: ', this.vector)
        console.log('--------- fin --------')
    }
}