import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  menu:string='';
  constructor() { }

  ngOnInit(): void {
    this.menu='menu-close'
  }
  menuOpenClose(menu:string){
    console.log('menu: ', menu)

    if(menu=='menu-open'){
      console.log('abrir')
      this.menu='menu-open'
    }else{
      console.log('cerrar')
      this.menu='menu-close'
    }
  }
  onclick(){
    
  }

}
