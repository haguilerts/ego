import { TestBed } from '@angular/core/testing';

import { AgenciaEgoService } from './agencia-ego.service';

describe('AgenciaEgoService', () => {
  let service: AgenciaEgoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AgenciaEgoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
