import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { CardEco } from '../shader/models/cardEgo';
@Injectable({
  providedIn: 'root'
})
export class AgenciaEgoService {
  private urlEgo:string
  constructor(private http:HttpClient) { 
     this.urlEgo='https://challenge.agenciaego.tech/api/models/' 
  }
  getAll():Promise<CardEco[]>{
    return this.http.get<CardEco[]>(this.urlEgo).toPromise();
    console.log("urlEgo: "+this.urlEgo)
  }

  getById(id:number):Promise<CardEco[]>{
    return this.http.get<CardEco[]>(`${this.urlEgo}?id=${id}`).toPromise();
  }
}
