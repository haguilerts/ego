export interface CardEco {
    id: number,
    name: string,
    segment: string,
    year: number,
    price: number,
    thumbnail: string,
    photo: string
}
  